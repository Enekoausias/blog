<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Signin Template for Ion Auth</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
  </head>
  <body class="text-center login" >
    <div class='form-signin'>
      <h1><?php echo lang('Auth.login_heading');?></h1>
      <p><?php echo lang('Auth.login_subheading');?></p>
      <div id="infoMessage"><?php echo $message;?></div>     
      <?php echo form_open('auth/login');?>
        <p>
          <?php echo form_label(lang('Auth.login_identity_label'), 'identity');?>
          <?php echo form_input($identity,set_value('identity'),['class'=>'form-control', 'required'=>'required', 'autofocus'=>'autofocus']);?>
        </p>
        <p>
          <?php echo form_label(lang('Auth.login_password_label'), 'password');?>
          <?php echo form_input($password, 'password', ['class'=>'form-control', 'required'=>'required']);?>
        </p>
        <p>
          <?php echo form_label(lang('Auth.login_remember_label'), 'remember');?>
          <?php echo form_checkbox('remember', '1', false, ['class'=>'form-control', 'id'=>'remember']);?>
        </p>
        <p><?php echo form_submit('submit', lang('Auth.login_submit_btn'),['class'=>'btn btn-lg btn-primary btn-block']);?></p>
      <?php echo form_close();?>
      <p><a href="forgot_password"><?php echo lang('Auth.login_forgot_password');?></a></p>
      </div>      
  </body>
</html>